import React, { useState, useEffect } from "react";
import { InputGroup, Form } from "react-bootstrap";
import { useHistory, useParams } from "react-router-dom";
import axios from "axios";
import Swal from "sweetalert2";

// method untuk membaca sistem dan bisa di ubah melalui input
export default function Edit() {
  const param = useParams();
  const [name, setName] = useState("");
  const [deskripsi, setDeskripsi] = useState("");
  const [harga, setHarga] = useState("");
  // const [kadarluarsa, setKadarluarsa] = useState("");
  const [img, setImg] = useState("");

  const history = useHistory();

  
  useEffect(() => {
    axios
      .get("http://localhost:3100/product/" + param.id)
      .then((response) => {
        const newList = response.data.data;
        setName(newList.name);
        setDeskripsi(newList.deskripsi);
        setHarga(newList.harga);
        setImg(newList.img);
        // setKadarluarsa(newList.kadarluarsa);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan Bre!" + error);
      });
  }, []);


  const updateList = async (e) => {
    e.preventDefault();

    const formData = new FormData();
    formData.append("file", img);
    formData.append("name", name);
    formData.append("deskripsi", deskripsi);
    formData.append("harga", harga);

    Swal.fire({
      title: "Yakin Untuk Mengedit",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yaa!",
    }).then((result) => {
      if (result.isConfirmed) {
        axios.put(
            "http://localhost:3100/product/" + param.id, formData,
            
            {
              headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`,
                "Content-Type": "multipart/form-data"
              },
            }
          )
          .then(() => {
            history.push("/daftar");
            Swal.fire("Berhasil", "Data kamu diUpdate", "success");
          })
          .catch((error) => {
            console.log(error);
          });
      }
    });
  };

  // const submitActionHandler = async (event) => {
  //   event.preventDefault();
  //   await
  //   Swal.fire({
  //     title: 'Yakin Untuk Mengedit',
  //     text: "You won't be able to revert this!",
  //     icon: 'warning',
  //     showCancelButton: true,
  //     confirmButtonColor: '#3085d6',
  //     cancelButtonColor: '#d33',
  //     confirmButtonText: 'Yaa!'
  //   }).then((result) => {
  //     if (result.isConfirmed) {
  //       axios
  //       .put("http://localhost:3100/product/" + param.id, {
  //         name: name,
  //         deskripsi: deskripsi,
  //         harga: harga,
  //         // kadarluarsa: kadarluarsa,
  //         img: img,
  //       })
  //       Swal.fire(
  //         'Deleted!',
  //         'Your file has been deleted.',
  //         'success'
  //       )
  //     }
  //   })
  //     .then(() => {
  //       Swal.fire(
  //           'Good job!',
  //           'You clicked the button!',
  //           'success'
  //         )
  //       history.push("/daftar");
  //     })
  //     .catch((error) => {
  //       alert("Terjadi Kesalahan" + error);
  //     });
  // };

  return (
    <div className="edit mx-5 text-white">
      <Form onSubmit={updateList}>
        <div className="container my-5">
          <div className="name mb-3">
            <Form.Label>
              <strong>Edit Barang</strong>
            </Form.Label>
            <InputGroup className="d-flex-gap-3">
              <Form.Control
                value={name}
                onChange={(e) => setName(e.target.value)}
              />
            </InputGroup>
          </div>

          <div className="place-of-birth mb-3">
            <Form.Label>
              <strong>Deskripsi</strong>
            </Form.Label>
            <InputGroup className="d-flex gap-3">
              <Form.Control
                value={deskripsi}
                onChange={(e) => setDeskripsi(e.target.value)}
              />
            </InputGroup>
          </div>

          <div className="place-date mb-3">
            <Form.Label>
              <strong>Harga</strong>
            </Form.Label>
            <div className="d-flex gap-3">
              <InputGroup className="d-flex gap-3">
                <Form.Control
                  type="text"
                  value={harga}
                  onChange={(e) => setHarga(e.target.value)}
                />
              </InputGroup>
            </div>
          </div>
          <div className="place-of-birth mb-3">
            {/* <Form.Label>
              <strong>img</strong>
            </Form.Label>
            <InputGroup className="d-flex gap-3">
              <Form.Control
                type="url"
                placeholder="Input img"
                value={img}
                onChange={(e) => setImg(e.target.value)}
              />
            </InputGroup> */}
            <Form.Group className="mb-3 text-white">
                <Form.Label>Image</Form.Label>
                <Form.Control
                  type="file"
                  placeholder="Input Image"
                  onChange={(e) => setImg(e.target.files[0])}
                  required
                />
              </Form.Group>
          </div>
          <div className="d-flex justify-content-end align-center mt-2">
            {/* type submit untuk sistem yang telah membaca method bahwa berupa edit */}
            <button className="buton btn  text-white" type="submit">
              Save
            </button>
          </div>
        </div>
      </Form>
    </div>
  );
}
