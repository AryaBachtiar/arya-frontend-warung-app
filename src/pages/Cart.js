import axios from "axios";
import React, { useState, useEffect } from "react";
import Swal from "sweetalert2";
// import "../css/Cart.css";
import { Button } from "react-bootstrap";
const Cart = ({ Makanan, setMakanan, handleChange }) => {
const [cart, setCart] = useState([]);
const [totalPage, setTotalpage] = useState(0);
const [pagenow, setPagenow] = useState(0)
  const getAll = (idx) => {
    axios
      .get("http://localhost:3100/cart?page=" + idx +"&usersId="+ localStorage.getItem("userId"))
      .then((res) => {
        setCart(res.data.data.content);
        setTotalpage(res.data.data.totalPages);
        setPagenow(idx);
      })
      .catch(() => {
        Swal.fire({
          icon: "error",
          title: "memunculkan data",
          showConfirmButton: false,
          timer: 1500,
        });
      });
  };
  const deleteCart = async (id) => {
    await Swal.fire({
      title: "Are you sure?",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    }).then(async(result) => { 
      if (result.isConfirmed) {
       await axios.delete("http://localhost:3100/cart/" + id);
        Swal.fire("Deleted!", "Your file has been deleted.", "success");
        getAll(0);
      }
    });
  };
  const chekout = async () => {
    await Swal.fire({
      title: "Yakin Untuk Chekout?",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, Checkout it!",
    }).then(async(result) => { 
      if (result.isConfirmed) {
       await axios.delete("http://localhost:3100/cart/checkout?usersId=" + localStorage.getItem("userId"));
        Swal.fire("Berhasil!", "Your Checkout.", "success");
        getAll(0);
      }
    });
  };
  const changePage = (e) => {
    getAll(e);
  };
  useEffect(() => {
    getAll(0);
  }, []);
  console.log(Makanan);

  const TotalHarga = cart.reduce(function (result, item) {
    return result + item.totalPrice;
  }, 0);
const konversionRupiah = (angka) => {
  return new Intl.NumberFormat("id-ID",{
    style: "currency",
    currency: "IDR",
    minimumFractionDigits: 0,
  }).format(angka);
};
function preview() {
 
  getAll(pagenow -1)
}
function nextpage() {
  getAll(pagenow +1)
}
  return (
    <div>
      <div>
        <table
          className="table table-bordered mx-10 my-5 "
          style={{ width: 1280 }}
        >
          <thead>
            <tr className="text-white">
              <th className="header">No</th>
              <th className="header">img</th>
              <th className="header">nama</th>
              <th className="header">Jumlah Menu</th>
              <th className="header">harga per Menu</th>
              <th className="header">total harga</th>
              <th className="header">Action</th>
            </tr>
          </thead>
          <tbody className="text-white">
            {cart.map((carts, index) => {
              return (
                <tr key={carts.id}>
                  <td>{index + 1}</td>
                  <td className="w-40">
                    <img src={carts.productId.img} alt="" />{" "}
                  </td>
                  <td>{carts.productId.name}</td>
                  <td>{carts.qty}</td>
                  <td>{konversionRupiah(carts.productId.harga)}</td>
                  <td>{konversionRupiah(carts.totalPrice)}</td>
                  <td>
                    <Button
                      variant="danger"
                      className="ml-2 text-white rounded-lg p-2 mb-3"
                      onClick={() => deleteCart(carts.id)}
                    >
                      Hapus
                    </Button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
        <div className="flex text-white">
          <div className="border-2 border-green-500 w-60 m-4 text-center ">
            <p>
              Total Harga:
              {konversionRupiah(TotalHarga)}
            </p>
          </div>
          <div className="pt-2">
            <button className="m-4 bg-amber-700 text-white rounded-lg p-2 hover:bg-amber-600 mb-3 "
            onClick={() => chekout()}
           >
              cekout
            </button>
          </div>
        </div>
        <ol class="flex justify-center gap-1 text-xs font-medium">
        <li>
          <span
            
            onClick={preview}
           
            class="inline-flex h-8 w-8 items-center justify-center rounded border border-gray-100 "
          >
         
            <svg
              xmlns="http://www.w3.org/2000/svg"
              class="h-3 w-3"
              viewBox="0 0 20 20"
              fill="currentColor"
            >
              <path
                fill-rule="evenodd"
                d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z"
                clip-rule="evenodd"
              />
            </svg>
          </span>
        </li>

        {createElements(totalPage, changePage)}
        <li>
          <span
     
           onClick={nextpage}
            class="inline-flex h-8 w-8 items-center justify-center rounded border border-gray-100"
          >
           
            <svg
              xmlns="http://www.w3.org/2000/svg"
              class="h-3 w-3"
              viewBox="0 0 20 20"
              fill="currentColor"
            >
              <path
                fill-rule="evenodd"
                d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                clip-rule="evenodd"
              />
            </svg>
          </span>
        </li>
      </ol>
      </div>
      
    </div>
    
  );
};
function createElements(n, clickPage) {
  var elements = [];
  for (let i = 0; i < n; i++) {
    elements.push(
      <li>
        <span
          onClick={() => clickPage(i)}
          key={i}
          class="cursor-pointer text-white block h-8 w-8 rounded border border-gray-100 text-center leading-8"
        >
          {i + 1}
        </span>
      </li>
    );
  }
  return elements;
}

export default Cart