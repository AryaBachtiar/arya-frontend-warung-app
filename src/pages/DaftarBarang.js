import { render } from "@testing-library/react";
import axios from "axios";
import React, { useEffect, useState } from "react";
import Button from "react-bootstrap/Button";
import { useHistory } from "react-router-dom";
import Swal from "sweetalert2/dist/sweetalert2.js";
import "./Daftar.css";
export default function Home() {
  const [barang, setBarang] = useState([]);
  const [totalPage, setTotalPage] = useState(0);
  const [cari, setCari] = useState("");
  const history = useHistory();

  const getAll = async (idx) => {
    console.log("Loading");

    await axios
      .get("http://localhost:3100/product/all?page=" + idx + "&search=" + cari)
      .then((res) => {
        setBarang(
          res.data.data.content.map((barang) => ({
            ...barang,
            qty: 1,
          }))
        );
        setTotalPage(res.data.data.totalPages);
      })
      .catch((error) => {
        alert("Terjadi keasalahan" + error);
      });
    console.log("End");
  };
  const changePage = (e) => {
    getAll(e);
  };

  useEffect(() => {
    getAll(0);
  }, []);
  const swalWithBootstrapButtons = Swal.mixin({
    customClass: {
      confirmButton: "btn btn-success",
      cancelButton: "btn btn-danger",
    },
    buttonsStyling: false,
  });

  const beli = (barang) => {
    axios.post("http://localhost:3100/cart", {
      productId: barang.id,
      qty: barang.qty,
      usersId: localStorage.getItem("userId"),
    });

    // Swal.fire(
    //   "Anda Menambahkan " + barang.nama,
    //   barang.nama + " Telah Ditambahkan ",
    //   "success"
    // );
  };
  const deleteUser = async (id) => {
    swalWithBootstrapButtons
      .fire({
        title: "Are you sure?",
        text: "You won't be able to revert this!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel!",
        reverseButtons: true,
      })
      .then((result) => {
        if (result.isConfirmed) {
          axios.delete("http://localhost:3100/product/" + id);
          swalWithBootstrapButtons.fire(
            "Deleted!",
            "Your file has been deleted.",
            "success"
          );
          // setTimeout(() => {
          // }, 1000);
          window.location.reload();
        } else if (result.dismiss === Swal.DismissReason.cancel) {
          swalWithBootstrapButtons.fire(
            "Cancelled",
            "Your imaginary file is safe :)",
            "error"
          );
        }
      });
    getAll();
  };

  const plusQuantityProduct = (idx) => {
    const listProduct = [...barang];
    if (listProduct[idx].qty >= 45) {
      return;
    }
    listProduct[idx].qty++;
    setBarang(listProduct);
  };
  const minusQuantityProduct = (idx) => {
    const listProduct = [...barang];
    if (listProduct[idx].qty <= 1) {
      return;
    }

    listProduct[idx].qty--;
    setBarang(listProduct);
  };

  const login = () => {
    history.push("/login");
  };
  return (
    <div className="container my-5">
      {/* Page Search */}
      <div className="flex justify-end">
        <form class="hidden lg:flex">
          <div class="relative">
            <input
              class="h-10 rounded-lg border-gray-200 pr-10 text-sm placeholder-gray-500 focus:z-10 text-center"
              placeholder="Search..."
              type="text"
              value={cari}
              onChange={(e) => setCari(e.target.value)}
            />

            <button
              onClick={(e) => {
                // Type Submit ditambahkan preventDefault jika diEnter akan mencari dan tidak akan realod
                e.preventDefault();
                getAll(0);
              }}
              type="submit"
              class="absolute inset-y-0 right-0 mr-px rounded-r-lg p-2 text-gray-600"
            >
              <span class="sr-only">Submit Search</span>
              <svg
                class="h-5 w-5"
                fill="currentColor"
                viewbox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  clip-rule="evenodd"
                  d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
                  fill-rule="evenodd"
                ></path>
              </svg>
            </button>
          </div>
        </form>
      </div>
      {/* End Serach */}
      <h1 className="text-white">DAFTAR MENU </h1>
      {barang.length !== 0 ? (
        <>
          <div className="grid grid-cols-4 gap-3">
            {barang.map((barang, idx) => {
              return (
                <p class="relative block overflow-hidden group">
                  <button class="absolute right-4 top-4 z-10 rounded-full bg-gray-900 p-1.5 text-red-600 transition hover:text-blue-700">
                    <span class="sr-only">Wishlist</span>

                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                      stroke-width="1.5"
                      stroke="currentColor"
                      class="w-4 h-4"
                    >
                      <path
                        stroke-linecap="round"
                        stroke-linejoin="round"
                        d="M21 8.25c0-2.485-2.099-4.5-4.688-4.5-1.935 0-3.597 1.126-4.312 2.733-.715-1.607-2.377-2.733-4.313-2.733C5.1 3.75 3 5.765 3 8.25c0 7.22 9 12 9 12s9-4.78 9-12z"
                      />
                    </svg>
                  </button>

                  <h2 class="mt-4 text-lg font-medium text-gray-200">
                    {barang.name}
                  </h2>

                  <p className="mt-1.5 text-sm text-white">
                    {barang.deskripsi}
                  </p>

                  <div class="relative p-6 bg-zinc-900 border border-gray-900 rounded-lg">
                    {/* <span class="whitespace-nowrap  px-3 py-1.5 text-xs font-medium text-white">
                      {barang.kadarluarsa}
                    </span> */}

                    <h3 class="mt-1.5 text-sm text-gray-300">
                      Rp.{barang.harga}
                    </h3>

                    <img
                      src={barang.img}
                      alt=""
                      class="rounded-lg object-cover w-full h-64 transition duration-500 group-hover:scale-105 sm:h-72"
                    />

                    {localStorage.getItem("role") === null ? (
                      <>
                        <br />
                        <button
                          onClick={login}
                          class="block w-full p-4 text-sm font-medium transition bg-yellow-400 rounded hover:scale-105"
                        >
                          Login Terlebih Dahulu
                        </button>
                      </>
                    ) : (
                      <>
                        {localStorage.getItem("role") === "ADMIN" ? (
                          <>
                            <br />

                            <button
                              onClick={() => deleteUser(barang.id)}
                              class="block w-full p-4 text-sm font-medium transition  bg-gradient-to-r from-red-400 via-red-600 to-red-800 rounded hover:scale-105 text-white"
                            >
                              Hapus
                            </button>
                            <br />
                            <a
                              className="no-underline text-black"
                              href={"/edit/" + barang.id}
                            >
                              <button class="block w-full p-4 text-sm font-medium transition bg-gradient-to-r from-yellow-300 via-yellow-500 to-yellow-700 text-white rounded hover:scale-105">
                                Ubah
                              </button>
                            </a>
                          </>
                        ) : (
                          <>
                            <br />
                            {/* Plus Minus */}
                            <div>
                              <label for="Quantity" class="sr-only">
                                {" "}
                                Quantity{" "}
                              </label>

                              <div class="flex items-center gap-1">
                                <button
                                  type="button"
                                  className={`w-20 h-5 leading-5 transition hover:opacity-75 ${
                                    barang.qty <= 1 ? "white" : "green"
                                  }`}
                                  onClick={() => minusQuantityProduct(idx)}
                                >
                                  -
                                </button>

                                <input
                                  type="number"
                                  id="Quantity"
                                  value={barang.qty}
                                  class=" font-mono h-7 w-20 rounded border-gray-200 text-center [-moz-appearance:_textfield] sm:text-sm 
      [&::-webkit-outer-spin-button]:m-0 [&::-webkit-outer-spin-button]:appearance-none
       [&::-webkit-inner-spin-button]:m-0 [&::-webkit-inner-spin-button]:appearance-none"
                                />

                                <button
                                  type="button"
                                  className={`w-20 h-5 leading-5 transition hover:opacity-75 ${
                                    barang.qty >= 45 ? "white" : "green"
                                  }`}
                                  onClick={() => plusQuantityProduct(idx)}
                                >
                                  +
                                </button>
                              </div>
                            </div>
                            <br />
                            <button
                              onClick={() => beli(barang)}
                              className="block w-full p-4 text-sm font-medium transition  bg-gradient-to-r from-blue-400 via-blue-600 to-blue-800 text-white rounded hover:scale-105"
                            >
                              Add to Cart
                            </button>
                          </>
                        )}
                      </>
                    )}
                  </div>
                </p>
              );
            })}
          </div>
          {/* Pagination */}
          <ol class="flex justify-center gap-1 text-xs font-medium">
            np
            {createElements(totalPage, changePage)}
            
          </ol>
          {/* End Pagination */}
        </>
      ) : (
        <>
          <h3 class="text-white">Makanan Tidak Ada </h3>
        </>
      )}
    </div>
  );
}

function createElements(n, clickPage) {
  var elements = [];
  for (let i = 0; i < n; i++) {
    elements.push(
      <li>
        <span
          onClick={() => clickPage(i)}
          key={i}
          class="cursor-pointer text-white block h-8 w-8 rounded border border-gray-100 text-center leading-8"
        >
          {i + 1}
        </span>
      </li>
    );
  }
  return elements;
}
