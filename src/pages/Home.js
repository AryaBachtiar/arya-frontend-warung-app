// import React, { useState } from "react";
// import Carousel from "react-bootstrap/Carousel";
import Carousel from 'react-bootstrap/Carousel';
import axios from "axios";
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import Swal from "sweetalert2/dist/sweetalert2.js";

export default function Home() {
  const [barang, setBarang] = useState([]);
  const [list, setList] = useState([]);
  const [totalPage, setTotalpage] = useState(0);
  const [cari, setCari] = useState("");
  const history = useHistory();

  const getAll = async (idx) => {
    await axios
      .get("http://localhost:3100/product/all?page=" + idx )
      .then((res) => {
        setList(res.data.data.content);
        setTotalpage(res.data.data.totalPages);
      })
      .catch((error) => {
        alert("Terjadi keasalahan" + error);
      });
  };
  const changePage = (e) => {
    getAll(e);
  };
  useEffect(() => {
    getAll(0);
  }, []);
  const swalWithBootstrapButtons = Swal.mixin({
    customClass: {
      confirmButton: "btn btn-success",
      cancelButton: "btn btn-danger",
    },
    buttonsStyling: false,
  });

  const deleteUser = async (id) => {
    swalWithBootstrapButtons
      .fire({
        title: "Are you sure?",
        text: "You won't be able to revert this!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel!",
        reverseButtons: true,
      })
      .then((result) => {
        if (result.isConfirmed) {
          axios.delete("http://localhost:3100/product/" + id);
          swalWithBootstrapButtons.fire(
            "Deleted!",
            "Your file has been deleted.",
            "success"
          );
          setTimeout(() => {
            window.location.reload();
          }, 1000);
        } else if (
          /* Read more about handling dismissals below */
          result.dismiss === Swal.DismissReason.cancel
        ) {
          swalWithBootstrapButtons.fire(
            "Cancelled",
            "Your imaginary file is safe :)",
            "error"
          );
        }
      });

    getAll(0);
  };

  const loginn = () => {
    history.push("/login");
  };

  return (
    <div className="container my-5" >
        <h1 className='text-white'>Selamat Datang Di Warung Online</h1>
        <br />
        <Carousel fade>
      <Carousel.Item>
        <img
          className="d-block w-100 h-80"
          src="https://accurate.id/wp-content/uploads/2021/01/warung-makan-1.jpg"
          alt="First slide"
        />
        <Carousel.Caption>
         
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100  h-80"
          src="https://cdn.idntimes.com/content-images/post/20171228/dscf5546-8c417efd89272b6d226313d6bb3509fe_600x400.jpg"
          alt="Second slide"
        />

        <Carousel.Caption>
         
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100  h-80"
          src="https://lelogama.go-jek.com/post_featured_image/General_1.jpg"
          alt="Third slide"
        />

        <Carousel.Caption>
          
        </Carousel.Caption>
      </Carousel.Item>
    </Carousel>
        <Carousel.Item>
          <img
            className="d-block w-100 h-80"
            src="https://bisnisukm.com/uploads/2020/06/manisnya-peluang-usaha-minuman-kekinian-modal-kecil.png"
            alt="Third slide"
          />
        </Carousel.Item>

        <br />

      {list.length !== 0 ? (
        <>
          <div className="grid grid-cols-4 gap-3">
            {list.map((makanan) => {
              return (
                <p class="relative block overflow-hidden group">
                  <button class="absolute right-4 top-4 z-10 rounded-full bg-gray-900 p-1.5 text-red-600 transition hover:text-blue-700">
                    <span class="sr-only">Wishlist</span>

                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                      stroke-width="1.5"
                      stroke="currentColor"
                      class="w-4 h-4"
                    >
                      <path
                        stroke-linecap="round"
                        stroke-linejoin="round"
                        d="M21 8.25c0-2.485-2.099-4.5-4.688-4.5-1.935 0-3.597 1.126-4.312 2.733-.715-1.607-2.377-2.733-4.313-2.733C5.1 3.75 3 5.765 3 8.25c0 7.22 9 12 9 12s9-4.78 9-12z"
                      />
                    </svg>
                  </button>

                
                  <div class="relative p-6 bg-zinc-900 border border-gray-900 rounded-lg">
                   
                      <h2 class="mt-4 text-lg font-medium text-gray-200">
                    {makanan.name}
                  </h2>

                  <p className="mt-1.5 text-sm text-white">
                    {makanan.deskripsi}
                  </p>


                    <h3 class="mt-1.5 text-sm text-gray-300">
                      Rp.{makanan.harga}
                    </h3>

                    <img
                      src={makanan.img}
                      alt=""
                      class="rounded-lg object-cover w-full h-64 transition duration-500 group-hover:scale-105 sm:h-72"
                    />

                
                  
                  </div>
                </p>
              );
            })}
          </div>
        </>
      ) : (
        <>
          <h1>Belum Ada Data </h1>
        </>
      )}
      <ol class="flex justify-center gap-1 text-xs font-medium">
      

        {createElements(totalPage, changePage)}
       
      </ol>
    </div>
  );
}
function createElements(n, clickPage) {
  var elements = [];
  for (let i = 0; i < n; i++) {
    elements.push(
      <li>
        <span
          onClick={() => clickPage(i)}
          key={i}
          class="cursor-pointer text-white block h-8 w-8 rounded border border-gray-100 text-center leading-8"
        >
          {i + 1}
        </span>
      </li>
    );
  }
  return elements;
}
