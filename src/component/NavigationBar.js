import React, { useEffect,useState } from "react";
import { Container, Nav, Navbar, Modal, Button, Form } from "react-bootstrap";
import axios from "axios";
import Swal from "sweetalert2/dist/sweetalert2.js";
import { useHistory } from "react-router-dom";
import { GoPerson } from "react-icons/go";
import userEvent from "@testing-library/user-event";

export default function NavigationBar() {
  const [show, setShow] = useState(false);
  const [show1, setShow1] = useState(false);
  const [name, setName] = useState("");
  const [deskripsi, setDeskripsi] = useState("");
  const [harga, setHarga] = useState("");
  const [img, setImg] = useState("");

  const [user, setUser] = useState([]);
 

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const handleClose1 = () => setShow1(false);
  const handleShow1 = () => setShow1(true);

  const history = useHistory();

  const addList = async (e) => {
    e.preventDefault();
    e.persist();
    const fromData = new FormData();
    fromData.append("file", img);
    fromData.append("name", name);
    fromData.append("harga", harga);
    fromData.append("deskripsi", deskripsi);

    try {
      await axios.post(
        "http://localhost:3100/product",
        fromData,

        // headers berikut berfungsi untuk method yang hanya diakses oleh admin
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
            "Content-Type": "multipart/form-data",
          },
        }
      );
      // Sweet Alert
      setShow(false);
      Swal.fire({
        icon: "success",
        title: "Berhasil menambahkan detail!",
        showConfirmButton: false,
        timer: 1500,
      });
      setTimeout(() => {
        window.location.reload();
      }, 1500);
    } catch (error) {
      console.log(error);
    }
  };

  // const addUser = async (e) => {
  //   e.preventDefault();
  //   Swal.fire("Good job!", "You clicked the button!", "success");
  //   const data = {
  //     name: name,
  //     deskripsi: deskripsi,
  //     kadarluarsa: kadarluarsa,
  //     harga: harga,
  //     img: img,
  //   };

  //   await axios
  //     .post("http://localhost:8000/daftarBarang" , data)
  //     .then(() => {
  //       setTimeout(() => {
  //         window.location.reload();
  //       }, 1000);
  //     })
  //     .catch((error) => {
  //       console.log(error);
  //     });
  // };
  const logout = () => {
    localStorage.clear();
    window.location.reload();
  };

  const getAll = async () => {
    await axios 
    .get("http://localhost:3100/user/" + localStorage.getItem("userId"))
    .then((res) => {
      setUser(res.data.data)
   
    })
    .catch((error) => {
      // alert("Terjadi kesalahan " + error);
    });
  };
  console.log(user)
  useEffect(() => {
    getAll();
  }, [])

  return (
    <div>
      <Navbar className="bg-black" expand="lg">
        <img
          src="https://media-cdn.tripadvisor.com/media/photo-s/0f/ba/63/fc/logo-general-d-sawah.jpg"
          alt=""
          style={{ width: 100 }}
        />
        <Container>
          <Navbar.Brand href="/" className="text-white font-serif ">
            D'Sawah
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav" text-color="white">
            <Nav className="me-auto text-white font-bold">
              <Nav.Link
                href="/daftar"
                className="text-white hover:text-blue-500 font-serif"
              >
                Daftar Menu
              </Nav.Link>
            </Nav>
            {localStorage.getItem("role") === "USER" && (
              <Nav.Link href="/cart" className="text-white">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  strokeWidth={1.5}
                  stroke="currentColor"
                  className="w-6 h-6"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M2.25 3h1.386c.51 0 .955.343 1.087.835l.383 1.437M7.5 14.25a3 3 0 00-3 3h15.75m-12.75-3h11.218c1.121-2.3 2.1-4.684 2.924-7.138a60.114 60.114 0 00-16.536-1.84M7.5 14.25L5.106 5.272M6 20.25a.75.75 0 11-1.5 0 .75.75 0 011.5 0zm12.75 0a.75.75 0 11-1.5 0 .75.75 0 011.5 0z"
                  />
                </svg>
              </Nav.Link>
            )}

            {localStorage.getItem("role") !== "ADMIN" ? (
              <></>
            ) : (
              <>
                <button className="btn text-white" onClick={handleShow}>
                  Add Barang
                </button>
              </>
            )}
            {localStorage.getItem("role") !== "USER" ? (
              <></>
            ) : (
              <>
                <button className="btn text-white" onClick={handleShow1}>
                  <h3>
                    {" "}
                    <GoPerson />{" "}
                  </h3>
                </button>
              </>
            )}
            {localStorage.getItem("role") !== null ? (
              <a className="btn text-white" onClick={logout} href="/">
                Logout
              </a>
            ) : (
              <a className="btn text-white" href="/login">
                login
              </a>
            )}
          </Navbar.Collapse>
        </Container>
      </Navbar>
      <Modal show={show} onHide={handleClose} animation={false}>
        <div class="bg-black text-white">
          <Modal.Header closeButton>
            <Modal.Title class="text-white">Tambah Barang</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form method="POST" onSubmit={addList}>
              <Form.Group className="mb-3 text-white">
                <Form.Label>Produk</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter name"
                  onChange={(e) => setName(e.target.value)}
                  value={name}
                  required
                />
              </Form.Group>
              <Form.Group className="mb-3 text-white">
                <Form.Label>Deskripsi</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter deskripsi"
                  onChange={(e) => setDeskripsi(e.target.value)}
                  value={deskripsi}
                  required
                />
              </Form.Group>
              <Form.Group className="mb-3 text-white">
                <Form.Label>Harga</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Input Hrga"
                  value={harga}
                  onChange={(e) => setHarga(e.target.value)}
                  required
                />
              </Form.Group>
              <Form.Group className="mb-3 text-white">
                <Form.Label>Image</Form.Label>
                <Form.Control
                  type="file"
                  placeholder="Input Image"
                  onChange={(e) => setImg(e.target.files[0])}
                  required
                />
              </Form.Group>
              {/* Untuk menghilangkan model */}
              <Button variant="primary" type="submit">
                Save
              </Button>
              ||
              <Button variant="secondary" onClick={handleClose}>
                Close
              </Button>
            </Form>
          </Modal.Body>
        </div>
      </Modal>

      {/* Profil */}
      <Modal show={show1} onHide={handleClose1} animation={false}>
        <div class="bg-gradient-to-br from-gray-500 via-gray-700 to-black rounded-lg no-underline">
          <Modal.Header closeButton></Modal.Header>
          <Modal.Body>
          <article class="rounded-xl bg-gray-800 p-4 bg-gradient-to-br from-gray-500 via-gray-700 to-gray-900">
            
  <div class="flex items-center">
    <img
      alt="Developer"
      src={user.foto}
      class="h-16 w-16 rounded-full object-cover"
    />

    <div class="ml-3">
      <h3 class="text-lg font-medium text-white">{user.nama}</h3>

      <div class="flow-root">
        <ul class="-m-1 flex flex-wrap">
          <li class="p-1 leading-none">
            <a href="#" class="text-xs font-medium text-gray-300 no-underline"> Twitter </a>
          </li>

          <li class="p-1 leading-none">
            <a href="#" class="text-xs font-medium text-gray-300 no-underline"> GitHub </a>
          </li>

          <li class="p-1 leading-none">
            <a href="#" class="text-xs font-medium text-gray-300 no-underline">Website</a>
          </li>
        </ul>
      </div>
    </div>
  </div>

  
  
  <div class="text-right">
  <a href="/profil">
    <Button>Detail</Button>
  </a>
  </div>
  <span class="absolute inset-x-0 bottom-0 h-2 bg-gradient-to-r from-purple-700 via-blue-600 to-purple-600"></span>
 
</article>
          </Modal.Body>
        </div>
      </Modal>
    </div>
  );
}
